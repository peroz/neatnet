# NEATnet

This is a simple test of a NEAT-based feedforward neural network on logical conjunction (OR). NEAT, and related algorithms, are part of an approach to artificial intelligence denominated "neuroevolution", which attemps to integrate biologically-inspired evolutionary algorithms into the overall field as an alternate (or complementary, in some cases) option when it comes to the creation and optimization of neural networks.

## Sources
***
* *Overview of the basic XOR example*. NEAT-Python docs. Available at: https://neat-python.readthedocs.io/en/latest/xor_example.html#getting-the-results; <br>

* *Efficient Evolution of Neural Network Topologies*. Kenneth O. Stanley and Risto Miikkulainen. Available at: http://nn.cs.utexas.edu/downloads/papers/stanley.cec02.pdf.
