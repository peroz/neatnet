"""
OR example using neuroevolution with the NEAT library

Based on: https://neat-python.readthedocs.io/en/latest/xor_example.html
"""

from __future__ import print_function
import os
import neat
import visualize

# OR inputs and expected outputs
or_inputs = [(0.0, 0.0), (0.0, 1.0), (1.0, 0.0), (1.0, 1.0)]
or_outputs = [(1.0, ), (0.0, ), (0.0, ), (1.0, )]

def eval_genomes(genomes, config):
    for genome_id, genome in genomes:
        genome.fitness = 4.0
        net = neat.nn.FeedForwardNetwork.create(genome, config)

        for xi, xo in zip(or_inputs, or_outputs):
            output = net.activate(xi)
            genome.fitness -= (output[0] - xo[0]) ** 2

def run(config_file):
    # Loading the experiment's configuration
    config = neat.Config(neat.DefaultGenome, 
                         neat.DefaultReproduction, 
                         neat.DefaultSpeciesSet,
                         neat.DefaultStagnation,
                         config_file)

    # Creating the population
    p = neat.Population(config)

    # Showing progress on the terminal
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(5))

    # Running up to 300 generations
    winner = p.run(eval_genomes, 300)

    # Printing the winning genome
    print(f"\nBest genome: {winner}\n")

    # Showing the output of the most fit genome against the training data
    print("\nOutput: \n")

    winner_net = neat.nn.FeedForwardNetwork.create(winner, config)

    for xi, xo in zip(or_inputs, or_outputs):
        output = winner_net.activate(xi)

        print(f"Input {xi}, expected output {xo}, got {output}")

    node_names = {-1: 'A', -2: 'B', 0: "A OR B"}

    visualize.draw_net(config, winner, True, node_names=node_names)
    visualize.plot_stats(stats, ylog=False, view=True)
    visualize.plot_species(stats, view=True)

    p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-4')
    p.run(eval_genomes, 10)

# Main function
if __name__ == "__main__":
    # Determining the path to the config file 
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, "../config/config-feedforward")
    run(config_path)